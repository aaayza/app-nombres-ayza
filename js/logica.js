

//modulos
//import express from 'express';
//const app = express();
//import cors from 'cors';

//formulario
const elForm = document.getElementById("elForm");


//console.log(document.forms[0]);
//console.log(document.miForm);
//console.log(document.forms[0]);
//console.log(document.miForm);

const manejarForm = (event) => {    
    console.log(`El nombre leído es ${elForm.elNombre.value}`);  
    event.preventDefault();
    averiguarPais(elForm.elNombre.value);
}

const handleFetch = async url => {
    const res = await fetch(url);
    return await handleError(res);
}

const handleError = (res) => {
    if (!res.ok) throw new Error(res.statusText);
    return res;
}

const getRespuestaHttp = (url, callback) => {
    const OK = 200;
    const DONE = 4;
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () { // Se usa function para que el this quede ligado
        if (this.readyState == DONE) {
            if (this.status == OK) {
                const resp = JSON.parse(this.responseText);
                callback(null, resp);
            } else
                callback(this.response ? JSON.parse(this.response) : {
                    error: "Desconocido"
                });
        }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
}

const averiguarPais = nombre => {
    let url = `https://api.nationalize.io/?name=${nombre}`;
    let paisMasProbable = getRespuestaHttp(url, (err, res) => {
        if (err)
            alert(`Error: ${err.error}`);
        else {
            let paisMasProb = res.country.reduce((a, b) => {
                return a.probability > b.probability ? a : b;
            }, 0);
            url = `https://restcountries.eu/rest/v2/alpha/${paisMasProb.country_id}`;
            getRespuestaHttp(url, (err, res) => {
                if (err) {
                    alert(`Error: ${err.error}`);
                    resultado.innerHTML = "";
                    document.getElementById("inputNombre").value = "";
                } else {
                    if (res.country.length === 0) {
                        alert('No se han encontrado resultados! :(');
                        resultado.innerHTML = "";
                        document.getElementById("inputNombre").value = "";
                    } else {
                        alert(`Ud probablemente sea de: ${paisMasProb.country}`);
                        
                        const pais = document.createElement("h1"); 
                        const flagImg = document.createElement("img");
                        flagImg.style.width = "300px";
                        flagImg.style.height = "200px";
                        
                        const descrip = document.createElement("p");
                        descrip.style.background = "black";
                        descrip.style.borderBlockColor = "transparent";
                        descrip.style.color = "cyan";
                        descrip.style.borderRadius = "20px";                                               
                        descrip.innerText = `País encontrado!`;
                             
                        const resultado = document.getElementById("resultado");
                        resultado.appendChild(pais);
                        resultado.appendChild(flagImg);
                        resultado.appendChild(descrip);
                    }
                }
            });
        }
    });
};

//elForm.onsubmit = manejarForm;
elForm.addEventListener("submit", manejarForm);